<ion-tabs class="tabs-icon-top tabs-stable">

  <ion-tab title="Orders" icon="ion-android-menu" href="/">
    <ion-nav-view name="home-tab"></ion-nav-view>
  </ion-tab>

  <ion-tab title="Stats" icon="ion-arrow-graph-up-right" href="#/tab/stats">
    <ion-nav-view name="stats-tab"></ion-nav-view>
  </ion-tab>

  <ion-tab title="Call Support" icon="ion-help-buoy" ui-sref="tabs.help">
    <ion-nav-view name="help-tab"></ion-nav-view>
  </ion-tab>

</ion-tabs>