<div class="list orders-list">
    <a ng-href="#/tab/order/{{order.$id}}" class="item fade-in mod" ng-repeat="order in orders" on-swipe-right="prioritize(order)" on-swipe-left="prioritize(order)" ng-class="{highlight: order.highlight}">
        <div class="progress-bg" ng-style="{width: (order.progress || 0) + '%'}" ng-if="order.progress < 100"></div>
        <div style="width:20%;float:right">
            <span ng-bind="(order.progress || 0) + '% complete'" style="float:right"></span>
            <span class="order-total" ng-bind="order.total.toFixed(2) + ' &euro;'"></span>
        </div>
        <div style="width:70%;float:left">
            <i class="icon" ng-class="{
                'ion-star': order.priority,
                'ion-record': !order.priority,

                assertive: order.status == 'incoming',
                balanced: order.status == 'ready',
                energized: order.status == 'processing',
                royal: order.status == 'hotcold',
                positive: order.status == 'delivered'
            }"></i>
            <span ng-bind="order.id"></span> -
            <span class="order-timestamp" ng-bind="order.from_now || 'few seconds ago'"></span>
        </div>
        <div style="width:70%;float:left">
            <span class="order-user-name" ng-bind="order.user.name"></span>
            - <span ng-bind="countItems(order) + ' items'"></span>
        </div>
    </a>
</div>