<ion-view>
    <ion-nav-title ng-bind="order.id"></ion-nav-title>
    <ion-content class="order">
        <h4 ng-bind="order.user.name" style="text-align:center"></h4>
        <h5 ng-bind="order.user.email" style="text-align:center"></h5>
        <h5 ng-bind="order.user.phone" style="text-align:center"></h5>
        <h4 class="order-status mod" ng-class="{
            assertive: order.status == 'incoming',
            balanced: order.status == 'ready',
            energized: order.status == 'processing',
            royal: order.status == 'hotcold',
            positive: order.status == 'delivered'
        }">
            <span ng-bind="'Status: ' + order.status"></span>
            <span class="complete-label" ng-bind="(order.progress || 0) + '% complete'"></span>
        </h4>
        <div class="list order-item-list">
            <a class="item item-avatar" href="" ng-repeat="item in order.items" on-swipe-right="deleteItem(item.ean)" on-tap="addItem(item.ean)" on-swipe-left="deleteItem(item.ean)" on-hold="showImage(item)" on-release="hideImage(item)">
              <img ng-src="{{'http://s3-eu-west-1.amazonaws.com/balticsimages/images/180x220/' + item.img + '.png'}}">
              <div class="order-item-quantity">
                <span ng-bind="(item.items_in_cart || 0) + '/' + item.q"></span>
                <i class="ion-ios-cart" ng-class="{
                    stable: !isItemFulfilled(item, order),
                    balanced: isItemFulfilled(item, order)
                }"></i>
              </div>
              <div class="order-item-info">
                <h4 ng-bind="item.title"></h4>
                <p ng-bind="'EAN:' + item.ean"></p>
                <p>
                    <span ng-bind="'&euro; ' + item.price.toFixed(2) + ' per ' + item.qty + ' ' + item.measure_unit.toUpperCase()"></span>
                    <i class="ion-ios-loop-strong" ng-show="item.exchange == 'Can Replace'"></i>
                </p>
              </div>
            </a>
        </div>
        <h4 ng-bind="'Total: ' + order.total.toFixed(2) + ' &euro;'" style="text-align:right; padding: 0 6px"></h4>
        <div class="button-bar">
            <button class="button button-light" ng-click="scanBarcode('add')">
                <i class="icon ion-ios-plus-outline"></i> Add <i class="icon ion-ios-barcode-outline"></i>
            </button>
            <button class="button button-light" ng-click="scanBarcode('delete')">
                <i class="icon ion-ios-minus-outline"></i> Remove <i class="icon ion-ios-barcode-outline"></i>
            </button>
        </div>

        <button class="button button-full button-positive" ng-click="toggleDelivered(order)" ng-if="order.status == 'ready' || order.status == 'hotcold'">Delivered/Picked up</button>
        <button class="button button-full button-assertive" ng-click="toggleDelivered(order)" ng-if="order.status == 'delivered'">Cancel</button>
        <button class="button button-full button-royal" ng-if="order.status == 'ready'" ng-click="toggleHotcold(order)">Has cold/warm items</button>
        <button class="button button-full button-assertive" ng-if="order.status == 'hotcold'" ng-click="toggleHotcold(order)">No cold/warm items in this order</button>
        <p class="delete-order"><a href="" ng-click="deleteOrder()">Delete Order</a></p>

    </ion-content>
</ion-view>