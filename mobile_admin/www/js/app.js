// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova', 'directives', 'services', 'Conf'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('tabs', {
      url: "/tab",
      abstract: true,
      templateUrl: "partials/tabs.tpl"
    })
    .state('tabs.home', {
      url: "/home",
      views: {
        'home-tab': {
          templateUrl: "partials/home.tpl",
          controller: 'HomeTabCtrl'
        }
      }
    })
    .state('tabs.order', {
      url: "/order/:orderId",
      views: {
        'home-tab': {
          templateUrl: "partials/order.tpl",
          controller: 'OrderTabCtrl'
        }
      }
    })
    .state('tabs.stats', {
      url: "/stats",
      views: {
        'stats-tab': {
          templateUrl: "partials/stats.tpl"
        }
      }
    })
    .state('tabs.help', {
      url: "/help",
      views: {
        'help-tab': {
          templateUrl: "partials/help.tpl"
        }
      }
    });
    $urlRouterProvider.otherwise("/tab/home");
})

.controller('HomeTabCtrl', function($scope) {
  console.log('HomeTabCtrl');
})

.controller('OrderTabCtrl', function($scope, $location, $stateParams, Orders, imagesBasePath, $cordovaBarcodeScanner, $ionicLoading, $ionicModal, $timeout, $ionicPopup) {
  console.log('OrderTabCtrl');
  $ionicLoading.show({
      template: 'loading'
  });
  Orders.admin.getOrders().$loaded(function(data){
    $scope.order = data.filter(function(order){
      return order.$id == $stateParams.orderId;
    })[0];
    $ionicLoading.hide();
  })
  $scope.imagesBasePath = imagesBasePath;
  $scope.findItem = function(ean){
    var item;
    for(var i = 0; i < $scope.order.items.length; i++){
      item = $scope.order.items[i];
      if(parseInt(item.ean) == parseInt(ean)){
        if(!item.items_in_cart){
          item.items_in_cart = 0;
        }
        return item;
      }
    }
  }
  $scope.addItem = function(ean){
    var item = $scope.findItem(ean);
    if(item){
      item.items_in_cart += 1;
      Orders.save($scope.order);
      $scope.updateOrderStatus();
    } else {
      alert('Item ' + ean + ' not found in order.');
    }
  }
  $scope.deleteItem = function(ean){
    var item = $scope.findItem(ean);
    if(item){
      if(item.items_in_cart > 0){
        item.items_in_cart -= 1;
        Orders.save($scope.order);
        $scope.updateOrderStatus();
      }
    } else {
      alert('Item ' + ean + ' not found in order.');
    }
  }
  $scope.scanBarcode = function(method) {
    $cordovaBarcodeScanner.scan().then(function(imageData) {
        var ean = imageData.text;
        if(method == 'add'){
          $scope.addItem(ean);
        } else {
          $scope.deleteItem(ean);
        }
    });
  }
  $scope.isItemFulfilled = function(item){
    if(!item.items_in_cart){
        return false;
    }
    return parseInt(item.items_in_cart) == parseInt(item.q);
  }
  $scope.getItemQStatus = function(item){
    if(!item.items_in_cart){
        item.items_in_cart = 0;
    }
    return {
      inCart: parseInt(item.items_in_cart),
      total: parseInt(item.q)
    }
  }
  $scope.updateOrderStatus = function(){
    $scope.order.status = 'processing';
    $scope.order.$priority = $scope.order.priority ? -1 : 0;
    var progress = {inCart: 0, total: 0};
    var item_progress;
    var fulfilled = true;
    angular.forEach($scope.order.items, function(item){
        if(!item.items_in_cart){
            item.items_in_cart = 0;
        }
        if(parseInt(item.items_in_cart) > parseInt(item.q)){
          item.items_in_cart = item.q;
        }
        item_progress = $scope.getItemQStatus(item);
        progress.inCart += item_progress.inCart;
        progress.total += item_progress.total;
        if(!$scope.isItemFulfilled(item)){
            fulfilled = false;
        }
    })
    if(fulfilled){
        $scope.order.status = 'ready';
        $scope.changePriority(2);
    }
    $scope.order.progress = Math.round(progress.inCart / progress.total * 100);
    if($scope.order.progress == 0){
      $scope.order.status = 'incoming';
      if($scope.order.$priority > 0){
        $scope.order.$priority = 0;
      }
    }
    Orders.save($scope.order);
    console.log('Order saved');
  }
  $scope.changePriority = function(priority){
    $scope.order.highlight = true;
    $timeout(function(){
      $scope.order.$priority = priority;
      delete($scope.order.highlight);
      Orders.save($scope.order);
    }, 2000);
  }
  $scope.toggleDelivered = function(){
      if($scope.order.status == 'delivered'){
          $scope.order.status = 'ready';
          $scope.order.$priority = 2;
      } else {
          $scope.order.status = 'delivered';
          $scope.order.$priority = 3;
      }
      Orders.save($scope.order);
  }
  $scope.toggleHotcold = function(){
      if($scope.order.status == 'hotcold'){
          $scope.order.status = 'ready';
          $scope.order.$priority = 2;
      } else {
          $scope.order.status = 'hotcold';
          $scope.order.$priority = 1;
      }
      Orders.save($scope.order);
  }
  $ionicModal.fromTemplateUrl('partials/zoom-image.tpl', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.zoom_modal = modal;
  });
  $scope.showImage = function(item){
    $scope.modalImage = item.img;
    $scope.zoom_modal.show();
  }
  $scope.hideImage = function(item){
    $scope.modalImage = item.img;
    $scope.zoom_modal.hide();
  }
  $scope.deleteOrder = function(){
    $ionicPopup.confirm({
      title: 'Confirm Delete',
      template: 'Are you sure you want to delete this order?'
    }).then(function(res) {
      if(res) {
        Orders.admin.orders.$remove($scope.order);
        $location.path('/');
      }
    });
  }
});