'use_strict'

angular.module('directives', [
    'Conf',
    'services'
])

.directive('orders', ['Shops', 'Orders', '$ionicLoading', function(Shops, Orders, $ionicLoading){
    return {
        replace: true,
        controller: ['$scope', '$timeout', function($scope, $timeout){
            $scope.prioritize = function(order) {
                if(!order.highlight){
                    if(order.$priority){
                        if(order.$priority == -1){
                            $scope.changePriority(order, 0);
                        }
                    } else {
                        $scope.changePriority(order, -1);
                    }
                    if(order.priority){
                        order.priority = false;
                    } else {
                        order.priority = true;
                    }
                    Orders.save(order);
                }
            }
            $scope.countItems = Orders.countItems;
            $scope.changePriority = function(order, priority){
                order.highlight = true;
                $timeout(function(){
                  order.$priority = priority;
                  delete(order.highlight);
                  Orders.save(order);
                }, 2000);
            }
            $scope.updateOrdersTimes = function(){
                angular.forEach($scope.orders, function(order){
                    order.from_now = Orders.fromNow(order.timestamp);                    
                })
            }
        }],
        templateUrl: 'partials/orders.tpl',
        link: function(scope, element, attrs){
            $ionicLoading.show({
                template: 'loading'
            });
            scope.orders = Orders.admin.getOrders();
            scope.orders.$loaded().then(function(){
                $ionicLoading.hide();
                scope.updateOrdersTimes();
            }).catch(function(error){

            })
            setInterval(function(){
                scope.updateOrdersTimes();
                scope.$apply();
            }, 60000);
            scope.shops = Shops.items;
        }
    }
}])
