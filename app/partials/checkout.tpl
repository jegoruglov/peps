<div style="width: 900px;margin:20px auto" ng-show="checkout">
  <h3 class="ui header">Our Terms</h3>
  <ol class="ui list">
    <li>
      Your package will be available for pickup 30 minutes after the ordering, if not specified otherwise during the ordering and on the order confirmation page (you will notice yelling notification).
    </li>
    <li>
      We guarantee food and beverages quality and freshness, reliable packaging and consistency of the order. In case of any problems please contact your cincierge by phone 59111119 or in place. We reserve the right to reject the complaint for subject of order consistency, so please check the items on concierge table. (if covered by insurence, then we'll satisfy the complaint).
    </li>
    <li>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </li>
    <li>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </li>
    <li>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </li>
  </ol>
  <div class="ui negative message" ng-show="error">
    <i class="close icon" ng-click="error = undefined"></i>
    <div class="header" ng-bind="error"></div>
  </div>
  <div class="ui checkbox" ng-click="agree = !agree">
    <input type="checkbox">
    <label>Makes sense, I agree with above terms and conditions.</label>
  </div>
  <div style="text-align:center; cursor:pointer" ng-click="pay()"><img ng-src="img/payments.png"/></div>
</div>