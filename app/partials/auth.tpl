<div class="right menu">
    <div ng-show="auth.loggedIn" class="ui dropdown item" style="padding-top: 5px;padding-bottom: 6px;">
        <i class="dropdown icon" ng-class="{img: auth.profile.img}"></i>
        <img ng-src="{{auth.profile.img}}" ng-show="auth.profile.img" style="border-radius: 15px;width: 25px;height: 25px;float: left;margin-right: 8px;"><span ng-bind="auth.profile.name" ng-class="{img: auth.profile.img != undefined}"></span>
        <div class="menu">
            <a class="item" ng-href="/user/settings"><i class="settings icon"></i>Settings</a>
            <a class="item" ng-href="user/profile"><i class="edit icon"></i>Profile</a>
        </div>
    </div>
    <a  ng-show="auth.loggedIn" class="ui item" ng-click="logout()">Logout</a>

    <a ng-hide="auth.loggedIn" class="ui item" ng-hide="auth.loggedIn" ng-click="showLogin()">Login</a>
    <a ng-hide="auth.loggedIn" class="ui item" ng-hide="auth.loggedIn" ng-click="showRegister()">Register</a>

    <div class="ui basic login modal">
        <div ng-show="loading" class="ui segment">
          <div class="ui active dimmer">
            <div class="ui text loader">Loading</div>
          </div>
        </div>
      <div class="content">
        <div style="width:300;margin:0 auto;text-align:center">
            <div class="ui red message" ng-bind="error" ng-show="error"></div>
            <div class="ui facebook button" ng-click="facebookLogin()" style="width: 100%">
              <i class="facebook icon"></i>
              Login with Facebook
            </div>
            <div class="ui google plus button" ng-click="googleLogin()" style="width: 100%;margin-top: 10px">
              <i class="google plus icon"></i>
              Login with Google
            </div>
            <div class="ui horizontal divider" style="color:white">
                Or
            </div>
            <div class="ui input" style="width: 100%">
                <input type="text" placeholder="email" ng-model="email" ng-enter="login()">
            </div>
            <div class="ui input" style="width: 100%;margin-top: 10px">
                <input type="password" placeholder="password" ng-model="password" ng-enter="login()">
            </div>
            <div class="ui button" style="width:100%;margin-top: 10px" ng-click="login()">Login</div>
            <div style="margin-top: 20px;">
                <a href="" style="color:rgb(124, 124, 124)" ng-click="hideLogin()"><i class="close icon"></i><span>Cancel</span></a>
            </div>
        </div>
      </div>
    </div>

    <div class="ui basic register modal">
        <div ng-show="loading" class="ui segment">
          <div class="ui active dimmer">
            <div class="ui text loader">Loading</div>
          </div>
        </div>
      <div class="content">
        <div style="width:300;margin:0 auto;text-align:center">
            <div class="ui red message" ng-bind="error" ng-show="error"></div>
            <div class="ui input" style="width: 100%">
                <input type="text" placeholder="email" ng-model="email" ng-enter="register()">
            </div>
            <div class="ui input" style="width: 100%;margin-top: 10px">
                <input type="password" placeholder="password" ng-model="password" ng-enter="register()">
            </div>
            <div class="ui green button" style="width:100%;margin-top: 10px" ng-click="register()">Register</div>
            <div style="margin-top: 20px;">
                <a href="" style="color:rgb(124, 124, 124)" ng-click="hideRegister()"><i class="close icon"></i><span>Cancel</span></a>
            </div>
        </div>
      </div>
    </div>

</div>