<div class="ui styled fluid accordion">
    <div ng-repeat-start="(shopId, list) in user.getUserLists()"></div>
    <div class="title" ng-class="{active: $index == 0}">
      <i class="dropdown icon"></i>
      <span ng-bind="shops[shopId].name"></span>
      <span style="font-size: 0.9em; color:grey; margin-left: 15px">
        <i class="marker icon"></i><span ng-bind="shops[shopId].adr"></span>
        <i class="call icon" style="margin-left: 10px"></i><span ng-bind="shops[shopId].phone"></span>
      </span>
    </div>
    <div class="content" style="margin-left: 20px" ng-class="{active: $index == 0}">
        <div class="ui list">
          <div class="item" ng-repeat="item in list.items">
            <div class="content">
              <a class="header" ng-bind="item.name" ng-href="#/shop/{{shopId}}" ng-click="list.selected = $index"></a>
              <div class="description">
                <span ng-bind="item.items.length + ' items, Total '"></span>
                <span ng-bind="'&euro; ' + item.total.toFixed(2)"></span>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div ng-repeat-end></div>
</div>