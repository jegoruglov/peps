<div class="ui segment" style="min-height: 500px">
    <div ng-show="loading" class="ui active inverted dimmer">
        <div class="ui text loader">Loading</div>
    </div>
    <div class="ui accordion">
        <div ng-repeat-start="(cat, cat_data) in categories[0]._source"></div>
        <div class="title">
            <i class="dropdown icon"></i>
            <span ng-bind="cat_data.name" ng-click="search(cat_data.name)"></span>
        </div>
        <div class="content" style="padding-left: 24px;">
            <div ng-repeat-start="(sub, sub_data) in cat_data.items"></div>
                <a ng-bind="sub_data.name" href="" ng-click="search(sub_data.name)"></a>
                <a class="ui circular mini label" ng-bind="sub_data.len"></a>
            <div ng-repeat-end></div>
        </div>
        <div ng-repeat-end></div>
    </div>
</div>