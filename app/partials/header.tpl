<div class="ui menu" style="position:fixed;width:100%;z-index:999">
  <div class="item" style="overflow: hidden;height: 30px">
    <img src="img/logo.png" style="top: -15px;height: 38px;position: relative">
  </div>
  <a class="green item" ng-href="#/about">
    About
  </a>
  <a class="red item" ng-href="#/">
    Shops
  </a>
  <a class="blue item" ng-show="auth.loggedIn" ng-href="#/lists">
    My Lists
  </a>
  <a class="orange item" ng-show="auth.loggedIn" ng-href="#/orders">
    My Orders
  </a>
  <div auth></div>
</div>