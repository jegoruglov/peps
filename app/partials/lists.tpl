<div class="ui top attached tabular menu">
    <a ng-repeat="list in user.getLists().items" class="item" ng-class="{active: $index == user.getLists().selected}" ng-click="selectList($index)" style="padding-right: 3px">
        <span ng-bind="list.name"></span>
        <i class="trash icon" style="margin-left: 10px" ng-show="$index == user.getLists().selected" ng-click="deleteList($index)"></i>
    </a>
    <a class="item" ng-click="createNewList()"><i class="plus icon"></i></a>
</div>
<div list class="ui bottom attached tab segment active"></div>