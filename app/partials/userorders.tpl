<div class="userorders">
  <div class="ui styled fluid accordion">
    <div ng-repeat-start="(shopId, orders) in orders"></div>
    <div class="title" ng-class="{active: $index == 0}">
      <i class="dropdown icon"></i>
      <span ng-bind="shops[shopId].name"></span>
      <span style="font-size: 0.9em; color:grey; margin-left: 15px">
        <i class="marker icon"></i><span ng-bind="shops[shopId].adr"></span>
        <i class="call icon" style="margin-left: 10px"></i><span ng-bind="shops[shopId].phone"></span>
      </span>
    </div>
    <div class="content" style="margin-left: 20px" ng-class="{active: $index == 0}">
      <div class="ui list">
        <div class="item" ng-repeat="order in orders" ng-click="goToOrder(order.$id)">
          <span class="ui label" ng-class="{
            red: order.status == 'incoming',
            green: order.status == 'ready' || order.status == 'hotcold',
            yellow: order.status == 'processing',
            blue: order.status == 'delivered'
          }" style="float: left;margin: 10px;font-weight: 100;" ng-bind="order.id"></span>
          <div class="content">
            <span class="header" ng-href="#/order/{{order.$id}}">
              <span ng-bind="{
                delivered: 'Picked up',
                incoming: 'In queue',
                processing: 'Processing',
                ready: 'Ready for pickup',
                hotcold: 'Ready for pickup'}[order.status]"></span>
              <span ng-bind="' - ' + order.progress + '% complete'" ng-if="order.status == 'processing'"></span>
            </span>
            <div class="description">
              <span ng-bind="'Placed ' + order.from_now + ', ' || 'few seconds ago, '"></span>
              <span ng-bind="order.items.length + ' items, Total '"></span>
              <span ng-bind="'&euro; ' + order.total.toFixed(2)"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-repeat-end></div>
  </div>
</div>