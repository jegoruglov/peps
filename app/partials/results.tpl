<div class="ui cards" infinite-scroll="scroll()" infinite-scroll-immediate-check="false" infinite-scroll-distance="1">
    <div ng-repeat-start="item in results"></div>
    <div item="item._source"></div>
    <div ng-repeat-end></div>
    <div class="ui centered inline loader" style="width:100%; margin: 20px" ng-class="{active: isLoading}"></div>
</div>