<div class="ui grid">
  
  <div class="one wide column"></div>
  <div class="fourteen wide column" style="margin-top:50px;">
    <h2 class="ui header">
      <span>Orders</span>
      <a class="ui red label" style="float:right">Unprocessed</a>
      <a class="ui yellow label" style="float:right">Processing</a>
      <a class="ui purple label" style="float:right">Ready but has cold/warm items</a>
      <a class="ui green label" style="float:right">Ready</a>
      <a class="ui blue label" style="float:right">Delivered/Picked up</a>
    </h2>
  </div>
  <div class="one wide column"></div>


  <div class="one wide column"></div>
  <div class="fourteen wide column">
    <div adminorders></div>
  </div>
  <div class="one wide column"></div>

</div>