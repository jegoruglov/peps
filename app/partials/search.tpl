<div class="ui search" ng-class="{loading: loading}">
    <div class="ui icon input" style="width: 100%">
        <input class="prompt" type="text" placeholder="Search..." ng-model="query" ng-model-options="{debounce: 250}" ng-change="search(query)">
        <i class="search icon"></i>
    </div>
    <div class="results" style="width: 100%"></div>
</div>