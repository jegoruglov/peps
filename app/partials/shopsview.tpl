<div class="ui grid">
    <style>
    .icon-shop::before, .icon-shop-1::before, .icon-truck::before {margin:0;}
    </style>
    <div class="four wide column"></div>
    <div class="eight wide column">
        <h1  style="margin:50px 0 20px 0" class="ui center aligned icon header"><i class="circular icon icon-shop"></i>Select Shop</h1>
        <div class="ui category search">
            <div class="ui icon input" style="width: 100%">
                <input class="prompt" type="text" placeholder="Search shop..." ng-model="query">
                <i class="search icon"></i>
            </div>
        </div>
    </div>
    <div class="four wide column"></div>

    <div class="two wide column"></div>
    <div class="twelve wide column">
        <div shops="shops" query="query"></div>
    </div>
    <div class="two wide column"></div>
</div>