<div class="ui stackable grid" style="background: rgb(52, 52, 52);color: rgb(218, 218, 218);padding:0 40px">
    <div class="four wide column">
        <ul class="footer" style="padding-top: 22px;">
            <li><a href="">About</a></li>
            <li><a href="">Terms</a></li>
            <li><a href="">Jobs</a></li>
        </ul>
    </div>
    <div class="eight wide column" style="text-align:center">
        <h1 class="ui header" style="color: #D3D3D3;padding-top: 26px;">You <span style="color:#e07b53">time</span> is Your most valuable resource!</h1>
        <p>3Peps OÜ, v<span ng-bind="version"></p>
    </div>
    <div class="four wide column">
        <ul style="float:right; text-align:right;list-style-type:none">
            <li><address>3Peps OÜ, Pikk tee 3, Tallinn, 12345, Estonia</address></li>
            <li><address>+372 625 6789</address></li>
            <li><address><a href="mailto:info@3peps.ee">info@3peps.ee</a></address></li>
        </ul>
    </div>
</div>