<div class="card" style="width: 135px; height: 218px; overflow:hidden;border-radius: 0;margin:3px">
    <div class="dimmable image" style="background: none; padding: 5px; min-height: 140px;cursor:pointer" ng-click="increment()">
      <div class="ui inverted dimmer">
        <div class="content">
          <div class="center">
            <i class="plus large icon"></i>
          </div>
        </div>
      </div>
      <img ng-show="item.img" ng-src="http://s3-eu-west-1.amazonaws.com/balticsimages/images/180x220/{{item.img}}.png" style="max-width: 125px; max-height: 125px;width: auto;margin: 0 auto;"/>
      <img ng-hide="item.img" ng-src="img/entry_no_image.png" style="max-width: 125px; max-height: 125px;width: auto;margin: 0 auto;"/>
    </div>
    <div class="content" style="height: 44px; padding:0 10px;overflow:hidden">
      <a class="header" style="font-size: 0.9em" ng-bind="item.title" ng-href="{{ item.url }}"></a>
      <div class="meta">
        <span class="date" ng-bind="item.qty + ' ' + item.measure_unit" style="font-size: 0.8em"></span>
      </div>
    </div>
    <a class="ui right red big corner label" ng-show="q > 0">
      <i class="cart icon"></i>
    </a>
    <div class="ui bottom attached label" style="background: white">
      <span ng-show="q > 0" ng-bind="q + ' in cart'" style="color:#d95c5c;position:absolute;bottom:10px"></span>
      <div ng-hide="item.original_price" ng-bind="'&euro; ' + item.price" style="float:right;font-size:1.4em"></div>
      <div ng-show="item.original_price" style="float:right">
        <span ng-bind="'&euro; ' + item.original_price" style="font-size: 1em;text-decoration: line-through;position:absolute;top:-3px;right:13px"></span>
        <span ng-bind="'&euro; ' + item.price" style="font-size: 1.4em;"></span>
      </div>
    </div>
</div>