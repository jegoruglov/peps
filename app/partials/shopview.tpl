<div class="ui grid" style="min-height: 600px">
    <div class="one wide column"></div>
    <div class="fourteen wide column">
        <div lists="lists" style="margin-top:50px"></div>
    </div>
    <div class="one wide column"></div>

    <div class="one wide column"></div>
    <div class="four wide column computer only">
         <div catalogue="catalogue" categories-index="categoriesIndex" index="index" type="type" limit="limit"></div>
    </div>
    <div class="ten wide column">
        <div search index="index" type="type" limit="limit"></div>
        <div results style="margin: 16px 0 0 6px"></div>
    </div>
    <div class="one wide column"></div>
</div>