<div class="ui styled fluid accordion">
    <div ng-repeat-start="order in orders"></div>
    <div class="title">
      <span style="margin-right:10px" class="ui label" ng-bind="fromNow(order.timestamp)"
      ng-class="{
        purple: order.status == 'hotcold',
        red: order.status == 'incoming',
        yellow: order.status == 'processing',
        green: order.status == 'ready',
        blue: order.status == 'delivered'
      }"></span>
      <span ng-bind="shops[order.shopId].name"></span> | 
      <span ng-bind="order.user.name"></span> | 
      <span ng-bind="order.user.email || 'anonimous'"></span> | 
      <span ng-bind="'&euro; ' + order.total.toFixed(2)"></span>
      | <span ng-bind="order.$priority"></span>
    </div>
    <div class="content">
        <table class="ui large very basic table" style=";margin:0 auto">
            <tbody>
              <tr ng-repeat="item in order.items" style="height:50px">
                <td class="two wide">
                  <i class="cart big icon" style="float:left" ng-class="{red: !isItemFulfilled(item, order), green: isItemFulfilled(item, order)}"></i>
                  <span class="ui transparent input">
                    <input type="number" placeholder="0" ng-model="item.items_in_cart" style="font-size:1.7em; text-align: center; width: 35px;" ng-change="updateOrderStatus(order)"/>
                  </span>
                </td>
                <td class="one wide" style="padding: 0 10px">
                  <img ng-show="item.img" ng-src="//s3-eu-west-1.amazonaws.com/balticsimages/images/180x220/{{item.img}}.png" style="max-width: 30px; max-height: 30px">
                  <img ng-hide="item.img" ng-src="img/entry_no_image.png" style="max-width: 30px; max-height: 30px"></td>
                <td class="six wide" ng-bind="item.title + ', ' + item.qty + ' ' + item.measure_unit.toUpperCase()"></td>
                <td class="one wide" style="padding:0">
                  <div class="exchange-toggle" ng-class="{active: item.exchange == 'Can Replace'}">
                    <p><i class="exchange icon"></i></p>
                    <p ng-bind="item.exchange"></p>
                  </div>
                </td>
                <td class="two wide right aligned" ng-bind="'&euro; ' + item.price.toFixed(2)"></td>
                <td class="one wide center aligned">x</td>
                <td class="one wide">
                    <div class="ui transparent input" ng-class="{error: item.q < 1}" ng-bind="item.q + ' ' + item.sales_unit.toLowerCase()"></div>
                </td>
                <td class="two wide center aligned" ng-bind="'&euro; ' + item.total.toFixed(2)"></td>
              </tr>
            </tbody>
        </table>
        <div style="margin:20px auto 0 auto">
          <button class="ui tiny blue button" ng-click="toggleDelivered(order)" ng-show="order.status == 'ready' || order.status == 'hotcold'">Delivered/Picked up</button>
          <button class="ui tiny red button" ng-click="toggleDelivered(order)" ng-show="order.status == 'delivered'">Cancel</button>
          <button class="ui tiny purple button" ng-show="order.status == 'ready'" ng-click="toggleHotcold(order)">Has cold/warm items</button>
           <button class="ui tiny red button" ng-show="order.status == 'hotcold'" ng-click="toggleHotcold(order)">No cold/warm items in this order</button>
        </div>
    </div>
    <div ng-repeat-end></div>
</div>