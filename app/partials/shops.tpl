<div class="ui segment" style="min-height: 500px">
    <div ng-hide="shops" class="ui active inverted dimmer">
        <div class="ui text loader">Loading</div>
    </div>
    <div class="ui cards">
        <div ng-repeat-start="shop in shops | filter:query"></div>
            <div shop="shop" user-list-count="userlists[shop.id]"></div>
        <div ng-repeat-end></div>
    </div>
    <iframe width="100%" height="600" style="margin-top:15px" src="https://www.youtube.com/embed/lHKG8vkB3_k" frameborder="0" allowfullscreen></iframe>
</div>