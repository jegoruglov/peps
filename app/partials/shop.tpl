<a class="card" ng-href="#/shop/{{shop.id}}" style="min-width: 236px; width:31%">
    <div class="content">
        <div class="header" ng-bind="shop.name"></div>
        <div class="meta" ng-bind="shop.phone"></div>
        <div class="description" ng-bind="shop.adr + ' | ' + shop.opened" style="min-height: 35px;"></div>
        <div class="floating ui teal label" style="top:-2px;left:98%" ng-show="userListCount" ng-bind="userListCount"></div>
    </div>
</a>