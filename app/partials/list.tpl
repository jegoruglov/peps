<div class="ui bottom attached active tab segment" ng-class="{loading: loading}" style="border: none">
  <h2 class="ui center aligned icon header" ng-show="!user.getSelectedList().items || user.getSelectedList().items.length == 0" style="color: rgb(218, 218, 218)">
    <i class="ban icon"></i>
    Empty List
  </h2>
  <table class="ui large very basic table" ng-show="user.getSelectedList().items.length > 0">
    <tbody>
      <tr ng-repeat="item in user.getSelectedList().items" style="height:50px" ng-class="{unchecked: !item.check}" class="item-row" ng-hide="!item.check && checkout">
        <td class="one wide" style="padding-left: 25px"><div class="check" ng-class="{checked: item.check}" ng-click="toggleCheck(item)"></div></td>
        <td class="one wide" style="padding: 0 10px" ng-click="toggleCheck(item)">
          <img ng-show="item.img" ng-src="//s3-eu-west-1.amazonaws.com/balticsimages/images/180x220/{{item.img}}.png" style="max-width: 30px; max-height: 30px">
          <img ng-hide="item.img" ng-src="img/entry_no_image.png" style="max-width: 30px; max-height: 30px"></td>
        <td class="six wide" ng-bind="item.title + ', ' + item.qty + ' ' + item.measure_unit.toUpperCase()" ng-click="toggleCheck(item)"></td>
        <td class="one wide" style="padding:0">
          <div class="exchange-toggle" ng-class="{active: item.exchange == 'Can Replace'}" ng-click="toggleExchange(item)">
            <p><i class="exchange icon"></i></p>
            <p ng-bind="item.exchange"></p>
          </div>
        </td>
        <td class="two wide right aligned" ng-bind="'&euro; ' + item.price.toFixed(2)" ng-click="toggleCheck(item)"></td>
        <td class="one wide center aligned" ng-click="toggleCheck(item)">x</td>
        <td class="two wide">
          <div style="width: 85px">
            <div class="ui basic mini button" style="margin: 0; padding:3px" ng-click="decrement(item)"><i class="minus icon" style="margin: 0"></i></div>
            <div class="ui transparent input" ng-class="{error: item.q < 1}"><input type="number" ng-model="item.q" style="text-align: center; width: 35px;" ng-blur="user.updateSelectedList()" ng-enter="user.updateSelectedList()"></div>
            <div class="ui basic mini button" style="margin: 0; padding:3px" ng-click="increment(item)"><i class="plus icon" style="margin: 0"></i></div>
          </div>
        </td>
        <td class="two wide center aligned" ng-bind="'&euro; ' + item.total.toFixed(2)" ng-click="toggleCheck(item)"></td>
      </tr>
      <tr ng-show="user.getSelectedList().items.length > 0">
        <td class="one wide"></td>
        <td class="one wide"></td>
        <td class="six wide"></td>
        <td class="one wide"></td>
        <td class="two wide"></td>
        <td class="one wide"></td>
        <td class="two wide right aligned">List Sub Total</td>
        <td class="two wide center aligned" ng-bind="'&euro; ' + (user.getSelectedList().total * 0.8).toFixed(2)"></td>
      </tr>
      <tr class="borderless" ng-show="user.getSelectedList().items.length > 0">
        <td class="one wide"></td>
        <td class="one wide"></td>
        <td class="six wide"></td>
        <td class="one wide"></td>
        <td class="two wide"></td>
        <td class="one wide"></td>
        <td class="two wide right aligned">VAT</td>
        <td class="two wide center aligned" ng-bind="'&euro; ' + (user.getSelectedList().total * 0.2).toFixed(2)"></td>
      </tr>
      <tr class="borderless" ng-show="user.getSelectedList().items.length > 0">
        <td class="one wide"></td>
        <td class="one wide"></td>
        <td class="six wide"></td>
        <td class="one wide"></td>
        <td class="two wide"></td>
        <td class="one wide"></td>
        <td class="two wide right aligned">List Total</td>
        <td class="two wide center aligned" ng-bind="'&euro; ' + user.getSelectedList().total.toFixed(2)"></td>
      </tr>
      <tr class="borderless" ng-show="type != 'checkout' && user.getSelectedList().items.length > 0">
        <td class="one wide"></td>
        <td class="one wide"></td>
        <td class="six wide">
          <div class="ui labeled input">
            <div class="ui label">Rename List</div>
            <input type="text" ng-model="user.getSelectedList().name" ng-blur="user.save()" ng-enter="user.save()">
          </div>
        </td>
        <td class="one wide"></td>
        <td class="two wide"></td>
        <td class="one wide"></td>
        <td class="two wide"></td>
        <td class="two wide center aligned">
          <a ng-hide="checkout" ng-click="checkout = !checkout" class="ui green button">Checkout</a>
          <a ng-show="checkout" ng-click="checkout = !checkout" class="ui red button">Cancel</a>
        </td>
      </tr>
    </tbody>
  </table>
  <div checkout="checkout"></div>
</div>