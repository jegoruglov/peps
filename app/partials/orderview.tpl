<div class="ui grid" style="min-height: 600px">
    
    <div class="sixteen wide column" style="margin-top: 50px">
        <h1 class="ui center aligned icon header">
          <!-- <i class="circular checkmark green icon"></i> -->
          <span ng-bind="shop.name"></span>
        </h1>
        <p style="text-align:center" ng-bind="shop_info"></p>
    </div>
    
    
    <div class="sixteen wide column">
      <div style="width:505px; margin:0 auto">
        <div id="qrcode"></div>
        <div style="padding:18px">
          <h2 class="ui header">Your order successfully placed</h2>
          <h3 class="ui header" style="margin: 0">It will be waiting for you after <span style="font-size:1.5em; color:#5bbd72">{{ ready_time }}</span></h3>
        </div>
      </div>
    </div>


    <div class="sixteen wide column" style="margin-top:20px">
        <table class="ui large very basic table" style="width: 1000px;margin:0 auto">
            <tbody>
              <tr ng-repeat="item in order.items" style="height:50px">
                <td class="one wide" style="padding-left: 25px"><div class="check" ng-class="{checked: item.check}"></div></td>
                <td class="one wide" style="padding: 0 10px">
                  <img ng-show="item.img" ng-src="//s3-eu-west-1.amazonaws.com/balticsimages/images/180x220/{{item.img}}.png" style="max-width: 30px; max-height: 30px">
                  <img ng-hide="item.img" ng-src="img/entry_no_image.png" style="max-width: 30px; max-height: 30px"></td>
                <td class="seven wide" ng-bind="item.title + ', ' + item.qty + ' ' + item.measure_unit.toUpperCase()"></td>
                <td class="one wide" style="padding:0">
                  <div class="exchange-toggle" ng-class="{active: item.exchange == 'Can Replace'}">
                    <p><i class="exchange icon"></i></p>
                    <p ng-bind="item.exchange"></p>
                  </div>
                </td>
                <td class="two wide right aligned" ng-bind="'&euro; ' + item.price.toFixed(2)"></td>
                <td class="one wide center aligned">x</td>
                <td class="one wide">
                    <div class="ui transparent input" ng-class="{error: item.q < 1}" ng-bind="item.q + ' ' + item.sales_unit.toLowerCase()"></div>
                </td>
                <td class="two wide center aligned" ng-bind="'&euro; ' + item.total.toFixed(2)"></td>
              </tr>
              <tr>
                <td class="one wide"></td>
                <td class="one wide"></td>
                <td class="seven wide"></td>
                <td class="one wide"></td>
                <td class="two wide"></td>
                <td class="one wide"></td>
                <td class="two wide right aligned">List Sub Total</td>
                <td class="two wide center aligned" ng-bind="'&euro; ' + (order.total * 0.8).toFixed(2)"></td>
              </tr>
              <tr class="borderless">
                <td class="one wide"></td>
                <td class="one wide"></td>
                <td class="seven wide"></td>
                <td class="one wide"></td>
                <td class="two wide"></td>
                <td class="one wide"></td>
                <td class="two wide right aligned">VAT</td>
                <td class="two wide center aligned" ng-bind="'&euro; ' + (order.total * 0.2).toFixed(2)"></td>
              </tr>
              <tr class="borderless">
                <td class="one wide"></td>
                <td class="one wide"></td>
                <td class="seven wide"></td>
                <td class="one wide"></td>
                <td class="two wide"></td>
                <td class="one wide"></td>
                <td class="two wide right aligned">List Total</td>
                <td class="two wide center aligned" ng-bind="'&euro; ' + order.total.toFixed(2)"></td>
              </tr>
            </tbody>
        </table>
    </div>


    <div class="sixteen wide column">
      <div style="width:555px; margin:0 auto" class="no-print">
        <div class="ui left action input">
          <button class="ui green labeled icon button">
            <i class="mail icon"></i>
              Send confirmation to
          </button>
          <input type="email" ng-model="confirmation_email" placeholder="email" style="width:250px;text-align:center">
        </div>
        <div class="ui green button" ng-click="print()" style="margin-left:10px">Print</div>
      </div>


    <div class="sixteen wide column" style="margin: 30px 0 100px 0;text-align:center">
      <p>If you have any questions, please contact your concierge at <span style="margin-left:5px; font-size: 1.1em"> 591 11119</span></p>
    </div>


</div>