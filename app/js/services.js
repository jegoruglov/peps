'use_strict';

angular.module('services', [
    'Conf',
    'firebase'
])

.factory('fbref', ['fbURL', function(fbURL){
    return new Firebase(fbURL);
}])

.factory('Auth', [
        '$q', '$firebase', '$firebaseAuth', 'fbref', '$rootScope', '$route',
        function($q, $firebase, $firebaseAuth, fbref, $rootScope, $route){
    var defer = $q.defer();
    var ref = fbref.child('users');
    var auth = $firebaseAuth(ref);
    var register = function(email, password){
        return auth.$createUser({email: email, password: password});
    }
    var login = function(email, password){
        return auth.$authWithPassword({email: email, password: password});
    }
    var loginWithFacebook = function(){
        return auth.$authWithOAuthPopup('facebook', {scope: 'email'});
    }
    var loginWithGoogle = function(){
        return auth.$authWithOAuthPopup('google', {scope: 'email'});
    }
    var loginAnonymously = function(){
        return auth.$authAnonymously();
    }
    var logout = function(){
        scope.profile.$destroy();
        auth.$unauth();
    }
    var getUserInfo = function(authData) {
        switch(authData.provider) {
            case 'password':
                return {
                    name: authData.password.email.replace(/@.*/, ''),
                    email: authData.password.email
                }
            case 'facebook':
                return {
                    provider: 'facebook',
                    name: authData.facebook.displayName,
                    email: authData.facebook.email,
                    img: authData.facebook.cachedUserProfile.picture.data.url
                }
            case 'google':
                return {
                    provider: 'google',
                    name: authData.google.displayName,
                    email: authData.google.email,
                    img: authData.google.cachedUserProfile.picture
                }
        }
    }
    auth.$onAuth(function(authData) {
        if (authData) {
            scope.profile = $firebase(ref.child(authData.uid)).$asObject();
            scope.profile.$loaded(function(data){
                if(authData.provider == 'anonymous'){
                    scope.loggedIn = false;
                    if(!scope.profile.name){
                        scope.profile.provider = 'anonymous';
                        scope.profile.name = 'anonymous';
                    }
                } else if(authData.provider == 'password'){
                    scope.loggedIn = true;
                    if(!scope.profile.name){
                        userData = getUserInfo(authData);
                        scope.profile.name = userData.name;
                        scope.profile.email = userData.email;
                        scope.profile.provider = 'password';
                    }
                } else {
                    scope.loggedIn = true;
                    if(!scope.profile.name){
                        userData = getUserInfo(authData);
                        scope.profile.name = userData.name;
                        scope.profile.email = userData.email;
                        scope.profile.provider = userData.provider;
                        scope.profile.img = userData.img;
                    }
                }
                scope.profile.$save();
                $rootScope.ready = true;
                defer.resolve();
                $route.reload();
            })
        } else {
            auth.$authAnonymously().then(function(data){
                $route.reload();
            }).catch(function(error){
                console.error('Unable to login anonymously.')
            })
        }
    });
    var scope = {
        loggedIn: false,
        profile: null,
        waitForAuth: defer.promise,
        register: register,
        login: login,
        loginWithFacebook: loginWithFacebook,
        loginWithGoogle: loginWithGoogle,
        loginAnonymously: loginAnonymously,
        logout: logout
    }
    return scope;
}])

.factory('User', [
        'Auth', '$firebase', '$routeParams', '$window',
        function(Auth, $firebase, $routeParams, $window){
    var getLists = function(){
        if(!Auth.profile.lists) {
            Auth.profile.lists = {};
        }
        if(!Auth.profile.lists[$routeParams.shopId]){
            Auth.profile.lists[$routeParams.shopId] = {
                selected: 0,
                items: [
                    {
                        name: 'untitled',
                        shopId: $routeParams.shopId,
                        items: []
                    }
                ]
            }
        }
        return Auth.profile.lists[$routeParams.shopId];
    }
    var getUserId = function(){
        return Auth.profile.$id;
    }
    var getUserImg = function(){
        return Auth.profile.img;
    }
    var getUserLists = function(){
        return Auth.profile.lists;
    }
    var getList = function(i){
        if(!getLists().items[i].items){
            getLists().items[i].items = [];
        }
        return getLists().items[i];
    }
    var getSelectedList = function(){
        return getList(getLists().selected);
    }
    var findInSelectedList = function(item){
        if(getSelectedList() && getSelectedList().items){
            for(var i = 0; i < getSelectedList().items.length; i++){
                if(getSelectedList().items[i].ean == item.ean){
                    return getSelectedList().items[i];
                }
            }
        }
    }
    var addToSelectedList = function(item){
        var i = findInSelectedList(item);
        if(i != undefined){
            if(item.q > 0){
                i.q = item.q;
            } else {
                deleteFromSelectedList(item);
                $window.scrollBy(0, -50);
            }
        } else {
            if(item.q > 0){
                getSelectedList().items.push(item);
                $window.scrollBy(0, 50);
            }
        }
        updateSelectedList();
    }
    var deleteFromSelectedList = function(item){
        for(var i = 0; i < getSelectedList().items.length; i++){
            if(getSelectedList().items[i].ean == item.ean){
                getSelectedList().items.splice(i, 1);
            }
        }
    }
    var updateSelectedList = function(){
        if(getSelectedList() && getSelectedList().items){
            var total = 0;
            var price;
            var itemTotal;
            angular.forEach(getSelectedList().items, function(item){
                if(item.q < 1){
                    deleteFromSelectedList(item);
                    return;
                }
                price = item.price;
                itemTotal = price * item.q;
                item.total = itemTotal;
                if(item.check) total += itemTotal;
            })
            getSelectedList().total = total;
            getSelectedList().items = _.sortBy(getSelectedList().items, function(item){
                return -item['check'];
            })
            save();
        }
    }
    var addOrder = function(orderId){
        if(!Auth.profile.orders){
            Auth.profile.orders = {};
        }
        Auth.profile.orders[orderId] = true;
        save();
    }
    var getOrdersIds = function(){
        if(!Auth.profile.orders){
            Auth.profile.orders = {};
        }
        var keys = [];
        for(var k in Auth.profile.orders) keys.push(k);
        return keys;
    }
    var getName = function(){
        return Auth.profile.name;
    }
    var getEmail = function(){
        return Auth.profile.email;
    }
    var save = function(){
        Auth.profile.$save();
    }
    var scope = {
        getUserId: getUserId,
        getUserImg: getUserImg,
        getLists: getLists,
        getUserLists: getUserLists,
        getList: getList,
        getSelectedList: getSelectedList,
        findInSelectedList: findInSelectedList,
        addToSelectedList: addToSelectedList,
        deleteFromSelectedList: deleteFromSelectedList,
        updateSelectedList: updateSelectedList,
        addOrder: addOrder,
        getOrdersIds: getOrdersIds,
        getName: getName,
        getEmail: getEmail,
        save: save
    }
    return scope;
}])

.factory('Orders', ['$firebase', 'fbref', 'User', '$routeParams', function($firebase, fbref, User, $routeParams){
    var createOrder = function(list){
        var list = angular.fromJson(angular.toJson(list));
        list.timestamp = Date.now();
        list.time = new Date(list.timestamp).toJSON().substring(0,10);
        list.status = 'incoming';
        list.id = scope.order_id.$value;
        scope.order_id.$value += 1;
        scope.order_id.$save();
        list.user = {
            id: User.getUserId(),
            img: User.getUserImg(),
            name: User.getName(),
            email: User.getEmail()
        }
        var l = angular.fromJson(angular.toJson(list));
        var items = [];
        angular.forEach(l.items, function(item){
            if(item.check){
                items.push(item);
            }
        })
        l.items = items;
        var new_object = scope.ref.push(l);
        new_object.setPriority(0);
        var orderId = new_object.key();
        User.addOrder(orderId);
        return orderId;
    }
    var getOrders = function(){
        var orderIds = User.getOrdersIds();
        var orders = [];
        angular.forEach(orderIds, function(orderId){
            var order = $firebase(scope.ref.child(orderId)).$asObject();
            orders.push(order);
        })
        return orders;
    }
    var getAdminOrders = function(){
        scope.admin.orders = $firebase(scope.ref).$asArray();
        return scope.admin.orders;
    }
    var printTime = function(timestamp){
        return new Date(timestamp).toJSON().substring(0,16);
    }
    var printDate = function(timestamp){
        return new Date(timestamp).toJSON().substring(0,10);
    }
    var fromNow = function(timestamp){
        return moment(parseInt(timestamp)).fromNow();
    }
    var save = function(order){
        scope.admin.orders.$save(order);
    }
    var scope = {
        ref: fbref.child('orders'),
        order_id: $firebase(fbref.child('order_id')).$asObject(),
        createOrder: createOrder,
        getOrders: getOrders,
        printTime: printTime,
        printDate: printDate,
        fromNow: fromNow,
        admin: {
            getOrders: getAdminOrders,
            orders: undefined
        },
        save: save
    }
    return scope
}])

.factory('Shops', ['$firebase', 'fbref', function($firebase, fbref){
    var scope = {
        items: $firebase(fbref.child('shops')).$asObject()
    }
    scope.items.$loaded(function(data){
        console.log('Shops loaded');
    })
    return scope;
}])

.factory('Results', function(){
    var scope = {
        items: [],
        scrollId: undefined,
        total: undefined,
        get: get,
        set: set
    }
    var get = function(){
        return scope.items;
    }
    var set = function(data){
        scope.items = data.hits.hits;
        scope.scrollId = data._scroll_id;
        scope.total = data.hits.total;
    }
    var getScrollId = function(){
        return scope.scrollId;
    }
    var isComplete = function(){
        return scope.total == scope.items.length;
    }
    var add = function(data){
        scope.items = scope.items.concat(data.hits.hits);
    }
    return {
        get: get,
        set: set,
        getScrollId: getScrollId,
        isComplete: isComplete,
        add: add
    }
})

.factory('Search', ['esURL', function(esURL){
    var client = new elasticsearch.Client({
        host: esURL
    });
    var query = function(index, type, query, size){
        fuzzy_q = '*' + query.split(' ').join('* *') + '*';
        var body = {
            query: {
                query_string: {
                    default_operator: 'AND',
                    query: '(' + query + ') OR (' + fuzzy_q + ')',
                    fields: ['title', 'tags', 'cat.name', 'sub.name']
                }
            }
        }
        if(size != undefined){
            body.size = size;
        }
        return client.search({
            scroll: '1m',
            index: index,
            type: type,
            body: body
        })
    };
    var scroll = function(scrollId){
        var scroll = {id: scrollId};
        return client.scroll({
            scrollId: scroll.id,
            scroll: '1m'
        })
    }
    var scope = {
        query: query,
        scroll: scroll,
        client: client
    }
    return scope;
}])

.factory('Catalogue', ['Search', function(Search){
    var get_categories = function(index, type){
        var body = {};
        return Search.client.search({
            index: index,
            type: type,
            body: body
        })
    }
    var query = function(index, type, query){
        var body = {
            size: 25,
            query: {
                filtered: {
                    query: {
                        query_string: {
                            query: '"' + query + '"'
                        }
                    },
                    filter: {
                        //term: {sub: {id: query}}
                    }
                }
            }
        }
        return Search.client.search({
            scroll: '1m',
            index: index,
            type: type,
            body: body
        })
    }
    var scope = {
        query: query,
        get_categories: get_categories
    }
    return scope;
}])

.factory('Cache', [function(){
    var duration = 60 * 60 * 1000;
    var setDuration = function(_duration_){
        duration = _duration_;
    }
    var put = function(k, v, d){
        try {
            if(d == undefined){
                d = duration;
            }
            try {
                sessionStorage[k] = angular.toJson({
                    expire: Date.now() + d,
                    data: v
                })
            } catch(e) {
                delete sessionStorage[k];
            }
        } catch(e) {}
    }
    var get = function(k){
        try {
            var v = angular.fromJson(sessionStorage[k]);
            if(v == undefined){ return; }
            if(v.expire > Date.now()){
                return v.data;
            } else {
                delete sessionStorage[k];
            }
        } catch(e) {}
    }
    return {
        put: put,
        get: get,
        setDuration: setDuration
    }
}])

