'use_strict';

angular.module('Peps', [
    'ngRoute',
    'services',
    'controllers',
    'directives',
    'infinite-scroll'
])

.config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/', {
        templateUrl: 'partials/shopsview.tpl',
        controller: 'ShopsCtrl',
        resolve: {
            auth: ['Auth', function(Auth) {
                return Auth.waitForAuth;
            }
        ]}
    }).when('/shop/:shopId', {
        templateUrl: 'partials/shopview.tpl',
        controller: 'ShopCtrl',
        resolve: {
            auth: ['Auth', function(Auth) {
                return Auth.waitForAuth;
            }
        ]}
    }).when('/lists', {
        templateUrl: 'partials/listsview.tpl',
        controller: 'ListsCtrl',
        resolve: {
            auth: ['Auth', function(Auth) {
                return Auth.waitForAuth;
            }
        ]}
    }).when('/orders', {
        templateUrl: 'partials/ordersview.tpl',
        controller: 'OrdersCtrl',
        resolve: {
            auth: ['Auth', function(Auth) {
                return Auth.waitForAuth;
            }
        ]}
    }).when('/order/:orderId', {
        templateUrl: 'partials/orderview.tpl',
        controller: 'OrderCtrl',
        resolve: {
            auth: ['Auth', function(Auth) {
                return Auth.waitForAuth;
            }
        ]}
    }).when('/admin', {
        templateUrl: 'partials/admin.tpl',
        controller: 'AdminCtrl',
        resolve: {
            auth: ['Auth', function(Auth) {
                return Auth.waitForAuth;
            }
        ]}
    }).otherwise({
        redirectTo: '/'
    });
}])