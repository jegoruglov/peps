'use_strict';

angular.module('controllers', [
    'services',
    'directives'
])

.controller('ShopsCtrl', ['$scope', 'Shops', function($scope, Shops){
    $scope.shops = [];
    var shops = Shops;
    shops.items.$loaded(function(data){
        angular.forEach(data, function(v){
            $scope.shops.push(v);
        })
        
    })
}])

.controller('ShopCtrl', ['$scope', '$routeParams', 'Auth', function($scope, $routeParams, Auth){
    $scope.index = 'catalogue';
    $scope.auth = Auth;
    $scope.shopId = $routeParams.shopId;
    $scope.type = $scope.shopId;
    $scope.categoriesIndex = 'categories'; 
    $scope.limit = 30;
}])

.controller('ListsCtrl', ['$scope', function($scope){
}])

.controller('OrdersCtrl', ['$scope', function($scope){
}])

.controller('CheckoutCtrl', ['$scope', '$routeParams', 'User', function($scope, $routeParams, User){
    $scope.user = User;
    $scope.shopId = $routeParams.shopId;
}])

.controller('OrderCtrl', ['$scope', '$routeParams', '$firebase', 'fbref', 'Shops', 'User', '$window', function($scope, $routeParams, $firebase, fbref, Shops, User, $window){
    $scope.qrcode = new QRCode('qrcode');
    $scope.orderId = $routeParams.orderId;
    $scope.confirmation_email = User.getEmail();
    var ref = fbref.child('orders').child($scope.orderId);
    $scope.print = function(){
        $window.print();
    }
    $firebase(ref).$asObject().$loaded(function(data){
        $scope.order = data;
        $scope.ready_time = new Date().toJSON().substring(11, 16);
        $scope.shop = Shops.items[data.shopId];
        $scope.shop_info = $scope.shop.adr + ' | Tel: ' + $scope.shop.phone + ' | ' + $scope.shop.opened;
        $scope.qrcode.makeCode(angular.toJson({
            id: data.$id,
            total: data.total,
            name: data.name,
            email: data.email
        }));
    });
}])

.controller('AdminCtrl', ['$scope', function($scope){

}])