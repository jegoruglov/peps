'use_strict'

angular.module('directives', [
    'Conf',
    'services'
])

.directive('footer', function(){
    return {
        controller: ['$scope', 'version', function($scope, version){
            $scope.version = version;
        }],
        templateUrl: 'partials/footer.tpl'
    }
})

.directive('header', function(){
    return {
        templateUrl: 'partials/header.tpl',
        link: function(scope, element, attrs){
            $(element).find('.dropdown').dropdown();
        }
    }
})

.directive('shop', function(){
    return {
        scope: {
            shop: '=',
            userListCount: '='
        },
        replace: true,
        templateUrl: 'partials/shop.tpl' 
    }
})

.directive('shops', ['User', function(User){
    return {
        scope: {
            shops: '=',
            query: '='
        },
        templateUrl: 'partials/shops.tpl',
        link: function(scope, element, attrs){
            var lists = User.getUserLists();
            scope.userlists = {};
            angular.forEach(lists, function(data, shopId){
                scope.userlists[shopId] = data.items.length;
            })
        }
    }
}])

.directive('auth', ['Auth', '$route', function(Auth, $route){
    return {
        replace: true,
        controller: ['$scope', function($scope){
            $scope.auth = Auth;
            $scope.email = null;
            $scope.password = null;
            $scope.loading = false;
            $scope.error;
            $scope.register = function(){
                $scope.loading = true;
                $scope.error = undefined;
                Auth.register($scope.email, $scope.password).then(function(data){
                    $scope.login();
                }).catch(function(error){
                    $scope.loading = false;
                    $scope.error = error.message;
                    console.error('Unable to register.');
                });
            }
            $scope.login = function(){
                $scope.loading = true;
                $scope.error = undefined;
                Auth.login($scope.email, $scope.password).then(function(data){
                    $scope.loading = false;
                    $scope.hideLogin();
                    $scope.hideRegister();
                }).catch(function(error){
                    $scope.loading = false;
                    $scope.error = error.message;
                    console.error('Unable to login.');
                });
            }
            $scope.facebookLogin = function(){
                $scope.loading = true;
                $scope.error = undefined;
                Auth.loginWithFacebook().then(function(authData) {
                    $scope.loading = false;
                    console.log("Logged in as:", authData.uid);
                    $scope.hideLogin();
                    $scope.hideRegister();
                }).catch(function(error) {
                    $scope.loading = false;
                    $scope.error = error.message;
                    console.error("Authentication failed:", error);
                });
            }
            $scope.googleLogin = function(){
                $scope.loading = true;
                $scope.error = undefined;
                Auth.loginWithGoogle().then(function(authData) {
                    $scope.loading = false;
                    console.log("Logged in as:", authData.uid);
                    $scope.hideLogin();
                    $scope.hideRegister();
                }).catch(function(error) {
                    $scope.loading = false;
                    $scope.error = error.message;
                    console.error("Authentication failed:", error);
                });
            }
            $scope.logout = function(){
                Auth.logout();
            }
            $scope.showLogin = function(){
                $scope.error = undefined;
                $('.basic.login.modal').modal('show');
            }
            $scope.hideLogin = function(){
                $scope.error = undefined;
                $('.basic.login.modal').modal('hide');
            }
            $scope.showRegister = function(){
                $scope.error = undefined;
                $('.basic.register.modal').modal('show');
            }
            $scope.hideRegister = function(){
                $scope.error = undefined;
                $('.basic.register.modal').modal('hide');
            }
        }],
        templateUrl: 'partials/auth.tpl',
        link: function(scope, element, attrs){
            $(element).find('.ui.dropdown').dropdown();
        }
    }
}])

.directive('lists', ['User', function(User){
    return {
        controller: ['$scope', function($scope){
            $scope.user = User;
            $scope.createNewList = function(){
                User.getLists().items.push({name:'untitled',items:[]});
                User.getLists().selected = User.getLists().items.length - 1;
                User.save();
            }
            $scope.deleteList = function(index){
                User.getLists().items.splice(index, 1);
                if(User.getLists().items.length == 0){
                    User.getLists().items.push({name:'untitled',items:[]});
                }
                User.getLists().selected = User.getLists().items.length - 1;
                User.save();
            }
            $scope.selectList = function(index){
                User.getLists().selected = index;
                User.save();
            }
        }],
        templateUrl: 'partials/lists.tpl'
    }
}])

.directive('list', ['User', function(User){
    return {
        scope: {
            type: '@'
        },
        controller: ['$scope', 'imagesBasePath', '$routeParams', function($scope, imagesBasePath, $routeParams){
            $scope.user = User;
            $scope.shopId = $routeParams.shopId;
            $scope.imagesBasePath = imagesBasePath;
            $scope.exchange = 'Can Be Replaced';
            $scope.toggleExchange = function(item){
                if(!item.exchange){
                    item.exchange = 'Can Replace';
                }
                if(item.exchange == 'Can Replace'){
                    item.exchange = "No Replace";
                } else {
                    item.exchange = 'Can Replace';
                }
                User.updateSelectedList();
            }
            $scope.decrement = function(item){
                if(item.q > 0){
                    item.q -= 1;
                }
                User.updateSelectedList();
            }
            $scope.increment = function(item){
                item.q += 1;
                User.updateSelectedList();
            }
            $scope.toggleCheck = function(item){
                item.check = !item.check;
                User.updateSelectedList();
            }
        }],
        templateUrl: 'partials/list.tpl',
        link: function(scope, element, attrs) {
            User.updateSelectedList();
        }
    }
}])

.directive('checkout', ['User', 'Orders', '$location', function(User, Orders, $location){
    return {
        scope: {
            checkout: '='
        },
        controller: ['$scope', function($scope){
            $scope.agree = false;
            $scope.pay = function(){
                if(!$scope.agree){
                    $scope.error = 'Please accept our terms and conditions.'
                } else {
                    var orderId = Orders.createOrder(User.getSelectedList());
                    $location.path('/order/' + orderId);
                }
            }
        }],
        templateUrl: 'partials/checkout.tpl',
        link: function(scope, element, attrs){
            $('.ui.checkbox').checkbox();
        }
    }
}])

.directive('item', ['User', function(User){
    return {
        scope: {
            item: '='
        },
        replace: true,
        controller: ['$scope', 'imagesBasePath', function($scope, imagesBasePath){
            $scope.imagesBasePath = imagesBasePath;
            $scope.q = 0;
            $scope.decrement = function(item){
                if($scope.q > 0){
                    $scope.q -= 1;
                    $scope.addToList();
                }
            }
            $scope.increment = function(item){
                $scope.q += 1;
                $scope.addToList();
            }
            $scope.addToList = function(){
                var item = angular.fromJson(angular.toJson($scope.item));
                item.check = true;
                item.exchange = 'Can Replace';
                item.q = $scope.q;
                User.addToSelectedList(item);
            }
            $scope.refreshState = function(){
                var i = User.findInSelectedList($scope.item);
                if(i != undefined){
                    $scope.q = i.q;
                } else {
                    $scope.q = 0;
                }
            }
        }],
        templateUrl: 'partials/item.tpl',
        link: function(scope, element, attrs){
            $(element).find('.image').dimmer({
              on: 'hover'
            });
            scope.$watch(function(){
                return User.getSelectedList().items;
            }, scope.refreshState, true);
            scope.refreshState();
        }
    }
}])

.directive('results', ['Results', 'Search', function(Results, Search){
    return {
        controller: ['$scope', function($scope){
            $scope.results = Results.get();
            $scope.scroll = function(){
                console.log('scroll');
                if(!Results.isComplete()){
                    $scope.isLoading = true;
                    Search.scroll(Results.getScrollId()).then(function(data){
                        Results.add(data);
                        $scope.isLoading = false;
                        $scope.$apply();
                    })
                }
            }
        }],
        templateUrl: 'partials/results.tpl',
        link: function(scope, element, attrs){
            scope.$watch(function(){
                return Results.get();
            }, function(){
                scope.results = Results.get();
            })
        }
    }
}])

.directive('search', [function(){
    return {
        scope: {
            index: '=',
            type: '=',
            limit: '='
        },
        controller: ['$scope', 'Search', 'Results', function($scope, Search, Results){
            $scope.query = '';
            $scope.search = function(query){
                if(query.length > 0){
                    $scope.loading = true;
                    Search.query($scope.index, $scope.type, query, $scope.limit).then(function(data){
                        Results.set(data);
                        $scope.loading = false;
                        $scope.$apply();
                    }).catch(function(error){
                        var a = 1;
                    });
                }
            }
        }],
        templateUrl: 'partials/search.tpl'
    }
}])

.directive('catalogue', ['Catalogue', 'Results', function(Catalogue, Results){
    return {
        scope: {
            index: '=',
            categoriesIndex: '=',
            type: '=',
            limit: '='
        },
        replace: true,
        controller: ['$scope', function($scope){
            $scope.loading = true;
            $scope.query = '';
            $scope.search = function(query){
                Catalogue.query($scope.index, $scope.type, query).then(function(data){
                    Results.set(data);
                    $scope.loading = false;
                    $scope.$apply();
                }).catch(function(error){
                    var a = 1;
                });
            }
        }],
        templateUrl: 'partials/catalogue.tpl',
        link: function(scope, element, attrs){
            $(element).accordion();
            Catalogue.get_categories(scope.categoriesIndex, scope.type).then(function(data){
                scope.categories = data.hits.hits;
                scope.loading = false;
                scope.$apply();
            }).catch(function(error){
                var a = 1;
            });
            scope.search('Piim');
        }
    }
}])

.directive('userlists', ['User', 'Shops', function(User, Shops){
    return {
        replace: true,
        templateUrl: 'partials/userlists.tpl',
        link: function(scope, element, attrs){
            scope.user = User;
            scope.shops = Shops.items;
            $(element).accordion();
        }
    }
}])

.directive('userorders', ['User', 'Shops', 'Orders', '$location', function(User, Shops, Orders, $location){
    return {
        replace: true,
        controller: ['$scope', function($scope){
            $scope.fromNow = Orders.fromNow;
            $scope.goToOrder = function(orderId){
                $location.path('/order/' + orderId);
            }
        }],
        templateUrl: 'partials/userorders.tpl',
        link: function(scope, element, attrs){
            scope.user = User;
            scope.shops = Shops.items;
            var orders = Orders.getOrders();
            scope.orders = {};
            angular.forEach(orders, function(order){
                order.$loaded(function(data){
                    if(!scope.orders[data.shopId]){
                        scope.orders[data.shopId] = [];
                    }
                    scope.orders[data.shopId].push(data);
                })
            })
            $(element).accordion();
        }
    }
}])

.directive('adminorders', ['Shops', 'Orders', function(Shops, Orders){
    return {
        replace: true,
        controller: ['$scope', function($scope){
            $scope.isItemFulfilled = function(item, order){
                if(!item.items_in_cart){
                    return false;
                }
                return parseInt(item.items_in_cart) == parseInt(item.q);
            }
            $scope.updateOrderStatus = function(order){
                order.status = 'processing';
                var fulfilled = true;
                angular.forEach(order.items, function(item){
                    if(!$scope.isItemFulfilled(item)){
                        fulfilled = false;
                    }
                })
                if(fulfilled){
                    order.status = 'ready';
                    order.$priority = 2;
                }
                Orders.save(order);
                console.log('Order saved');
            }
            $scope.toggleDelivered = function(order){
                if(order.status == 'delivered'){
                    order.status = 'ready';
                    order.$priority = 0;
                } else {
                    order.status = 'delivered';
                    order.$priority = 3;
                }
                Orders.save(order);
            }
            $scope.toggleHotcold = function(order){
                if(order.status == 'hotcold'){
                    order.status = 'ready';
                    order.$priority = 2;
                } else {
                    order.status = 'hotcold';
                    order.$priority = 1;
                }
                Orders.save(order);
            }
            $scope.fromNow = Orders.fromNow;
        }],
        templateUrl: 'partials/adminorders.tpl',
        link: function(scope, element, attrs){
            Orders.admin.getOrders().$loaded(function(data){
                scope.orders = data;
            })
            scope.shops = Shops.items;
            $(element).accordion();
        }
    }
}])

.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                        scope.$eval(attrs.ngEnter);
                });
                
                event.preventDefault();
            }
        });
    };
})